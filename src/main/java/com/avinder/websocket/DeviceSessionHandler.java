
package com.avinder.websocket;

import com.avinder.model.User;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
@Scope("application")
public class DeviceSessionHandler extends TextWebSocketHandler {
    private int userId = 0;
    Set<User> users = new HashSet<>();
    Logger logger = Logger.getLogger(DeviceSessionHandler.class.getName());

    private static final CopyOnWriteArrayList<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
    public void addSession(WebSocketSession session) {
        sessions.add(session);
        // adding all users to new session
        for (User user : users) {
            JSONObject addMessage = createAddMessage(user);
            sendToSession(session, addMessage);
        }
    }

    public void removeSession(WebSocketSession session) {
        sessions.remove(session);
    }

    public List<User> getUsers() {
        return new ArrayList<>(users);
    }

    public void addUser(User user) {
        user.setId(userId);
        users.add(user);
        userId++;
        JSONObject addMessage = createAddMessage(user);
        sendToAllConnectedSessions(addMessage);
    }

    public void removeUser(int id) {
        User user = getUserById(id);
        if (user != null) {
            users.remove(user);
            JSONObject removeMessage = new JSONObject(){{
                put("action","remove");
                put("id",id);
            }};
            sendToAllConnectedSessions(removeMessage);
        }
    }

    public void toggleUser(int id) {

        User user = getUserById(id);
        if (user != null) {
            if ("On".equals(user.getStatus())) {
                user.setStatus("Off");
            } else {
                user.setStatus("On");
            }
            JSONObject updateDevMessage = new JSONObject(){{
                put("action", "toggle");
                put("id",id);
                put("status",user.getStatus());
            }};
            sendToAllConnectedSessions(updateDevMessage);
        }
    }

    private User getUserById(int id) {

        for (User user: users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    private JSONObject createAddMessage(User user) {
        return new JSONObject(){{
             put("action", "add");
             put("id", user.getId());
             put("name", user.getName());
             put("type", user.getType());
             put("status", user.getStatus());
             put("description", user.getDescription());
        }};
    }

    private void sendToAllConnectedSessions(JSONObject message) {
        for (WebSocketSession session : sessions) {
            sendToSession(session, message);
        }
    }

    private void sendToSession(WebSocketSession session, JSONObject message) {
        try {
            session.sendMessage(new TextMessage(message.toString()));
        } catch (IOException ex) {
            sessions.remove(session);
            Logger.getLogger(DeviceSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("session opened: "+session.getId()+" "+session.toString());
        this.addSession(session);
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        System.out.println("WebSocketMessage !!!!!!!!!!!!!!!!!!!!!!!"+message+"       ");
        JSONObject jsonMessage = new JSONObject(message.getPayload().toString());
        System.out.println("jsonMessage !!!!!!!!!!!!!!!!!!!!!!!"+jsonMessage);
        System.out.println("handle message ");
        if ("add".equals(jsonMessage.getString("action"))) {
            User user = new User();
            user.setName(jsonMessage.getString("name"));
            user.setDescription(jsonMessage.getString("description"));
            user.setType(jsonMessage.getString("type"));
            user.setStatus("Off");
            this.addUser(user);
        }

        if ("remove".equals(jsonMessage.getString("action"))) {
            int id = (int) jsonMessage.getInt("id");
            this.removeUser(id);
        }

        if ("toggle".equals(jsonMessage.getString("action"))) {
            int id = (int) jsonMessage.getInt("id");
            this.toggleUser(id);
        }
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable error) throws Exception {
        System.out.println("session error");
        logger.log(Level.SEVERE, null, error);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        System.out.println("session closed");
        this.removeSession(session);
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
